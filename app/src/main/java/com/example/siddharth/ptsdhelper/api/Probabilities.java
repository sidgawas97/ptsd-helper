package com.example.siddharth.ptsdhelper.api;

public class Probabilities {
    private float negative;
    private float positive;
    private float neutral;

    public float getNegative() {
        return negative;
    }

    public float getPositive() {
        return positive;
    }

    public float getNeutral() {
        return neutral;
    }

    public void setNegative(float negative) {
        this.negative = negative;
    }

    public void setPositive(float positive) {
        this.positive = positive;
    }

    public void setNeutral(float neutral) {
        this.neutral = neutral;
    }
}
