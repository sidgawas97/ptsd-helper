package com.example.siddharth.ptsdhelper;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.ResultReceiver;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.siddharth.ptsdhelper.api.Api;
import com.example.siddharth.ptsdhelper.api.EmotionResult;
import com.example.siddharth.ptsdhelper.api.EmotionResultWrapper;
import com.example.siddharth.ptsdhelper.api.Result;
import com.example.siddharth.ptsdhelper.database.Memo;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;

public class tts_memo extends AppCompatActivity implements View.OnClickListener {

    private Result sentimentResult;
    private EmotionResultWrapper emotionResult;
    EditText speech ;
    ImageButton micimage;
    private TextView resultView;
    FloatingActionButton fab,fab2,fab3;
    private Api apiService;
    private Call<Result> resultCall;
    private Call<EmotionResultWrapper> emotionResultCall;
    Animation fabopen,fabclose,clockwise,anticlockwise;
    boolean isOpen=false;
    private Button analyzeButton= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tts_memo);
        resultView = findViewById(R.id.textView);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://apis.paralleldots.com/v3/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService = retrofit.create(Api.class);
        resultCall = null;
        speech= (EditText) findViewById(R.id.speech);
        micimage = (ImageButton) findViewById(R.id.mic);
        analyzeButton =(Button) findViewById(R.id.analyse);
        micimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speechinput();

            }
        }
        );
        analyzeButton.setOnClickListener(this);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) findViewById(R.id.fab3);

        fabopen= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_open);
        fabclose= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        clockwise= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        anticlockwise= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.offrotate);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isOpen)
                {
                    fab2.startAnimation(fabclose);
                    fab3.startAnimation(fabclose);

                    fab.startAnimation(anticlockwise);
                    fab2.setClickable(false);
                    fab3.setClickable(false);


                }
                else
                {
                    fab2.startAnimation(fabopen);
                    fab3.startAnimation(fabopen);

                    fab.startAnimation(clockwise);
                    fab2.setClickable(true);
                    fab3.setClickable(true);



                }
                isOpen=!isOpen;

            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speech.setText("");
                resultView.setText("");

            }
        });

        fab3.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.analyse)
        {
            analyseText();
        }
        else if(v.getId() == R.id.fab3)
        {
            saveToDB();
        }
    }

    private void saveToDB()
    {

    }
    private void analyseText()
    {
        emotionResultCall = apiService.getEmotions(speech.getText().toString(),Api.KEY);
        resultCall = apiService.getPredictions(speech.getText().toString(),Api.KEY);

        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();
                sentimentResult = result;
                String s = resultView.getText()+"\n"+"Setiment = "+result.getSentiment();
                resultView.setText(s);


            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Toast.makeText(tts_memo.this, "Error"+t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        emotionResultCall.enqueue(new Callback<EmotionResultWrapper>() {
            @Override
            public void onResponse(Call<EmotionResultWrapper> call,
                                   Response<EmotionResultWrapper> response) {
                emotionResult = response.body();
                String s = resultView.getText()+"\n"+"Emotion = "+emotionResult.getEmotion().getEmotion();
                resultView.setText(s);

                if(emotionResult.getEmotion().getEmotion().toUpperCase().equals("HAPPY")
                        || emotionResult.getEmotion().getEmotion().toUpperCase().equals("EXCITED"))
                {
                    SharedPreferences p = getSharedPreferences("APP_PREF",MODE_PRIVATE);
                    p.edit().putBoolean("SHOW_QUOTE",false).commit();
                }
                else
                {
                    SharedPreferences p = getSharedPreferences("APP_PREF",MODE_PRIVATE);
                    p.edit().putBoolean("SHOW_QUOTE",true).commit();
                }
            }

            @Override
            public void onFailure(Call<EmotionResultWrapper> call, Throwable t) {
                Toast.makeText(tts_memo.this, "Error"+t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    /*public void onButtonclick(View v)
        {
            if(v.getId()==R.id.mic)
            {

                speechinput();
            }
        }
    */
    public void speechinput()
    {
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        //i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        i.putExtra(RecognizerIntent.EXTRA_PROMPT,"Say Something");


        try{
            startActivityForResult(i,100);
        }
        catch (ActivityNotFoundException a){
            Toast.makeText(this, "Not supported", Toast.LENGTH_SHORT).show();
        }
    }
    public void onActivityResult(int req_code,int result_code,Intent i)
    {
        super.onActivityResult(req_code,result_code,i);

        switch(req_code)
        {
            case 100: if(result_code == RESULT_OK && i!=null)
            {
                ArrayList<String> result = i.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                speech.setText(speech.getText().toString() + result.get(0) + ". ");
                speech.setSelection(speech.getText().length());

            }
            break;

        }
    }
}
