package com.example.siddharth.ptsdhelper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.siddharth.ptsdhelper.api.Api;
import com.example.siddharth.ptsdhelper.api.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.
                create()).baseUrl("http://apis.paralleldots.com/v3/").
                build();
        Api api = retrofit.create(Api.class);
        Call<Result> resultCall = api.getPredictions("Hi i am noob, I am bad",Api.KEY);
        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();
                Log.d("RESPONSE", "onResponse: "+result.getSentiment());

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }
}
