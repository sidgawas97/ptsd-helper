package com.example.siddharth.ptsdhelper.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity
public class Memo {
    @PrimaryKey(autoGenerate = true)
    private int mid;
    @ColumnInfo(name = "memo_date")
    private Date memoDate;
    @ColumnInfo(name = "memo_text")
    private String  memoText;
    @ColumnInfo(name = "negative_score")
    private float negativeScore;
    @ColumnInfo(name = "neutral_score")
    private float neutralScore;
    @ColumnInfo(name = "positive_score")
    private float positiveScore;

    @ColumnInfo(name = "excited")
    private float excited;
    @ColumnInfo(name = "angry")
    private float angry;
    @ColumnInfo(name = "sad")
    private float sad;
    @ColumnInfo(name = "happy")
    private float happy;
    @ColumnInfo(name = "fear")
    private float fear;
    @ColumnInfo(name = "bored")
    private float bored;

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public Date getMemoDate() {
        return memoDate;
    }

    public void setMemoDate(Date memoDate) {
        this.memoDate = memoDate;
    }

    public String getMemoText() {
        return memoText;
    }

    public void setMemoText(String memoText) {
        this.memoText = memoText;
    }

    public float getNegativeScore() {
        return negativeScore;
    }

    public void setNegativeScore(float negativeScore) {
        this.negativeScore = negativeScore;
    }

    public float getNeutralScore() {
        return neutralScore;
    }

    public void setNeutralScore(float neutralScore) {
        this.neutralScore = neutralScore;
    }

    public float getPositiveScore() {
        return positiveScore;
    }

    public void setPositiveScore(float positiveScore) {
        this.positiveScore = positiveScore;
    }

    public float getExcited() {
        return excited;
    }

    public void setExcited(float excited) {
        this.excited = excited;
    }

    public float getAngry() {
        return angry;
    }

    public void setAngry(float angry) {
        this.angry = angry;
    }

    public float getSad() {
        return sad;
    }

    public void setSad(float sad) {
        this.sad = sad;
    }

    public float getHappy() {
        return happy;
    }

    public void setHappy(float happy) {
        this.happy = happy;
    }

    public float getFear() {
        return fear;
    }

    public void setFear(float fear) {
        this.fear = fear;
    }

    public float getBored() {
        return bored;
    }

    public void setBored(float bored) {
        this.bored = bored;
    }
}
