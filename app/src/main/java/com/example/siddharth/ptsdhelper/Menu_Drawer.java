package com.example.siddharth.ptsdhelper;


import android.content.Intent;
import android.net.Network;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

public class Menu_Drawer extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;

    CalendarView calendarView;
    TextView setdate;

    Button micbtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__drawer);







        //getSupportActionBar().setTitle("Home");

// first fragment
    /*    FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.container,frag_home.class,R.);
        transaction.addToBackStack(null);
        transaction.commit();*/




        drawerLayout = findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView = findViewById(R.id.nv);

        setupdrawercontent(navigationView);



        getSupportFragmentManager().beginTransaction().replace(R.id.flcontent,new frag_home()).commit();


    }
    public boolean onOptionsItemSelected(MenuItem item){
        if (actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void selectitemdrawer(MenuItem menuItem,NavigationView navigationView)
    {
        Fragment myfrag = null;
        Class fragclass;
        switch (menuItem.getItemId()){

            case R.id.first:
                myfrag = new frag_home();
                getSupportFragmentManager().beginTransaction().replace(R.id.flcontent,myfrag).commit();
                break;

            case R.id.second:
                myfrag = new frag_aid();
                getSupportFragmentManager().beginTransaction().replace(R.id.flcontent,myfrag).commit();
                break;
            case R.id.forth:
                myfrag = new frag_help();
                getSupportFragmentManager().beginTransaction().replace(R.id.flcontent,myfrag).commit();
                break;
            case R.id.fifth:
                myfrag = new frag_med();
                getSupportFragmentManager().beginTransaction().replace(R.id.flcontent,myfrag).commit();
                break;

            default:
                fragclass = frag_home.class; break;
        }
        /*try{
            myfrag = (Fragment) fragclass.newInstance();
        }
        catch (Exception e)
        {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flcontent,myfrag).commit();*/
        for (int i = 0; i < navigationView.getMenu().size(); i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }



        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        drawerLayout.closeDrawers();

    }
    private void setupdrawercontent(final NavigationView navigationView){
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectitemdrawer(item, navigationView);
                return true;
            }
        });
    }

}
