package com.example.siddharth.ptsdhelper.api;

import com.google.gson.annotations.SerializedName;

public class EmotionProbabilities {
    @SerializedName("Angry")
    private float angry;
    @SerializedName("Sad")
    private float sad;
    @SerializedName("Fear")
    private float fear;
    @SerializedName("Bored")
    private float bored;
    @SerializedName("Excited")
    private float excited;
    @SerializedName("Happy")
    private float happy;

    public float getAngry() {
        return angry;
    }

    public void setAngry(float angry) {
        this.angry = angry;
    }

    public float getSad() {
        return sad;
    }

    public void setSad(float sad) {
        this.sad = sad;
    }

    public float getFear() {
        return fear;
    }

    public void setFear(float fear) {
        this.fear = fear;
    }

    public float getBored() {
        return bored;
    }

    public void setBored(float bored) {
        this.bored = bored;
    }

    public float getExcited() {
        return excited;
    }

    public void setExcited(float excited) {
        this.excited = excited;
    }

    public float getHappy() {
        return happy;
    }

    public void setHappy(float happy) {
        this.happy = happy;
    }
}
