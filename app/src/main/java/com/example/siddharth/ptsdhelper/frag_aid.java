package com.example.siddharth.ptsdhelper;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link frag_aid.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link frag_aid#newInstance} factory method to
 * create an instance of this fragment.
 */
public class frag_aid extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SeekBar ans1;
    public SeekBar ans2;
    public SeekBar ans3;
    public SeekBar ans4;
    public SeekBar ans5;
    public Button submit;
    public TextView res;
    public TextView res_text;
    public int score;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ans1= getView().findViewById(R.id.a1);
        ans2= getView().findViewById(R.id.a2);
        ans3= getView().findViewById(R.id.a3);
        ans4= getView().findViewById(R.id.a4);
        ans5= getView().findViewById(R.id.a5);
        submit=getView().findViewById(R.id.submit_btn);
        res=getView().findViewById(R.id.result);
        res_text=getView().findViewById(R.id.result_text);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                score = ans1.getProgress()+ans2.getProgress()+ans3.getProgress()+ans4.getProgress()+ans5.getProgress();
                if(score> 20)
                {
                    res_text.setTextColor(Color.RED);
                res.setText("Your score is "+ Integer.toString(score)+ ".");
                res_text.setText("You SHOULD Seek Help!!");
            }
            else if (score >10 && score<20)
                {
                    res_text.setTextColor(Color.CYAN);
                    res.setText("Your score is "+ Integer.toString(score)+ ".");
                    res_text.setText("You may meet criteria for PSTD diagnosis");
                }
                else
                {
                    res_text.setTextColor(Color.GREEN);
                    res.setText("Your score is "+ Integer.toString(score)+ ".");
                    res_text.setText("YAAAY!! You're PTSD free");
                }
            }
        });


    }

    private OnFragmentInteractionListener mListener;

    public frag_aid() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment frag_aid.
     */
    // TODO: Rename and change types and number of parameters
    public static frag_aid newInstance(String param1, String param2) {
        frag_aid fragment = new frag_aid();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_frag_aid, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
