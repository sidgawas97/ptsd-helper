package com.example.siddharth.ptsdhelper.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {
    static String KEY = "mcNh14zGoYh87Yi8AbPfH0NWaCvdqvLXZhjFfml78LQ";

    @POST("sentiment")
    Call<Result> getPredictions(@Query("text") String text, @Query("api_key") String api_key);
    @POST("emotion")
    Call<EmotionResultWrapper>getEmotions(@Query("text") String text, @Query("api_key") String api_key);
}
