package com.example.siddharth.ptsdhelper.api;

public class EmotionResultWrapper{
    private EmotionResult emotion;

    public EmotionResult getEmotion() {
        return emotion;
    }

    public void setEmotion(EmotionResult emotion) {
        this.emotion = emotion;
    }
}
