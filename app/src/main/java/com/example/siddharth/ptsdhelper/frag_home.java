package com.example.siddharth.ptsdhelper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link frag_home.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link frag_home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class frag_home extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    CalendarView calendarView;
    TextView setdate;
    MediaPlayer mp;
    TextView intialtime,endtime;
    SeekBar position;
    int totaltime;
    ImageButton play;

    Animation Open;

    TextView textview1,textview2;
    View view1;
    LinearLayout linearLayout;





    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setdate = getView().findViewById(R.id.date);
        calendarView = getView().findViewById(R.id.calendar);

        intialtime = getView().findViewById(R.id.start);
        endtime = getView().findViewById(R.id.end);
        position = (SeekBar) getView().findViewById(R.id.player);
        play = (ImageButton) getView().findViewById(R.id.playbtn);

        mp = MediaPlayer.create(getActivity(),R.raw.music);
        mp.setLooping(true);
        mp.seekTo(0);
        totaltime =  mp.getDuration();

        position.setMax(totaltime);

        Open= AnimationUtils.loadAnimation(getActivity(),R.anim.fab_open);

        textview1 = getView().findViewById(R.id.textviewcnt1);
        textview2 = getView().findViewById(R.id.textviewcnt3);
        view1 = getView().findViewById(R.id.view);
        linearLayout = getView().findViewById(R.id.Linearlayoutcnt3);







        position.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if(b)
                {
                    mp.seekTo(i);
                    position.setProgress(i);
                }



            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mp.isPlaying())
                {
                    mp.start();
                    play.setBackgroundResource(R.drawable.ic_pause_black_24dp);
                }
                else
                {
                    mp.pause();
                    play.setBackgroundResource(R.drawable.ic_play_arrow_black_24dp);
                }
            }
        });









        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                //setdate.setText(" "+i2+"/"+i1+"/"+i+" ");
                startActivity(new Intent(getActivity(),tts_memo.class));
            }
        });




    }

    private OnFragmentInteractionListener mListener;

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences p = getContext().getSharedPreferences("APP_PREF",Context.MODE_PRIVATE);
        boolean show = p.getBoolean("SHOW_QUOTE",false);
        if(show)
        {
            textview1.setVisibility(View.VISIBLE);
            textview2.setVisibility(View.VISIBLE);

            textview1.startAnimation(Open);
            textview2.startAnimation(Open);
            view1.startAnimation(Open);
            linearLayout.startAnimation(Open);
        }
        else
        {
            textview1.setVisibility(View.INVISIBLE);
            textview2.setVisibility(View.INVISIBLE);
        }

    }

    public frag_home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment frag_home.
     */
    // TODO: Rename and change types and number of parameters
    public static frag_home newInstance() {
        frag_home fragment = new frag_home();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }



    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_frag_home, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
