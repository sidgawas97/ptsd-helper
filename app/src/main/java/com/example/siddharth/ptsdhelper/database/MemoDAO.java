package com.example.siddharth.ptsdhelper.database;

import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.Date;
import java.util.List;

public interface MemoDAO {
    @Insert()
    public void insertMemos(Memo ... memos);

    @Insert()
    public void insertMemo(Memo memo);

    @Query("SELECT * FROM memo")
    public List<Memo> getAllMemos();

    @Query("SELECT * FROM memo WHERE memo_date= :date")
    public List<Memo>getMemo(Date date);
}
