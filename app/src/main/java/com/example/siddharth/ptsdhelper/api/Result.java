package com.example.siddharth.ptsdhelper.api;

public class Result {
    private Probabilities probabilities;
    private String sentiment;
    private int code;

    public Probabilities getProbabilities() {
        return probabilities;
    }

    public String getSentiment() {
        return sentiment;
    }

    public int getCode() {
        return code;
    }

    public void setProbabilities(Probabilities probabilities) {
        this.probabilities = probabilities;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
