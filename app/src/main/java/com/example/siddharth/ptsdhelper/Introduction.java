package com.example.siddharth.ptsdhelper;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Introduction extends AppCompatActivity {

    ViewPager mpager;
    private LinearLayout dots_layout;
    private ImageView[] dots;
    int[] layouts = {R.layout.slide1, R.layout.slide2, R.layout.slide3};
    private Mpager mpageradapter;
    Button btnnext,btnskip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);

        mpager = (ViewPager) findViewById(R.id.viewpager);
        mpageradapter = new Mpager(layouts, this);
        mpager.setAdapter((mpageradapter));
        dots_layout = (LinearLayout) findViewById(R.id.dots);
        btnnext=(Button) findViewById(R.id.btnnext);
        btnskip=(Button) findViewById(R.id.btnskip);
        createdots(0);

        getSupportActionBar().hide();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadnextslid();


            }
        });
        btnskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Introduction.this,Menu_Drawer.class));
                finish();
            }
        });

        mpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                createdots(i);
                if(i==layouts.length-1)
                {
                    btnnext.setText("Start");
                    btnskip.setVisibility(View.INVISIBLE);
                }
                else
                {
                    btnnext.setText("Next");
                    btnskip.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }


    private void createdots(int current_positon) {
        if (dots_layout != null)
            dots_layout.removeAllViews();

        dots = new ImageView[layouts.length];

        for (int i = 0; i < layouts.length; i++) {
            dots[i] = new ImageView(this);
            if (i == current_positon) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.activedots));
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.defaultdots));
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(4, 0, 4, 0);
            dots_layout.addView(dots[i], params);

        }

    }

   /* @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnnext:
            {
                loadnextslid();
                break;
            }
            case R.id.btnskip:
            {
                startActivity(new Intent(this,Intoduction.class));
                finish();
                break;
            }
        }
    }*/

    public void loadnextslid()
    {
        int nextslide = mpager.getCurrentItem()+1;

        if(nextslide<layouts.length)
        {
            mpager.setCurrentItem(nextslide);
        }
        else
        {
            startActivity(new Intent(Introduction.this,Menu_Drawer.class));
            finish();
        }
    }
}
