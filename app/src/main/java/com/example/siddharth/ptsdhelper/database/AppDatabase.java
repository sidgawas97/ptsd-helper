package com.example.siddharth.ptsdhelper.database;

import android.arch.persistence.room.RoomDatabase;

public abstract class AppDatabase extends RoomDatabase {
    public abstract MemoDAO memoDAO();
}
