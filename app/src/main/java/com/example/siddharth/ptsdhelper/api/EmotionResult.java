package com.example.siddharth.ptsdhelper.api;

public class EmotionResult {
    private String emotion;
    private EmotionProbabilities probabilities;
    private int code;

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public EmotionProbabilities getProbabilities() {
        return probabilities;
    }

    public void setProbabilities(EmotionProbabilities probabilities) {
        this.probabilities = probabilities;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
